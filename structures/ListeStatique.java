//Entete
/**************************************************************************
 * ListeStatique
 **************************************************************************
 * PROGRAMMEUR(S) (ETUDIANT(ES)):
 * @author G. Brunet, F. Dufresne, S. Fortin, C. Potvin
 *
 * COURS: INF111-01
 * TRAVAIL/PROJET: TP02
 * NUMERO: Partie 02
 *
 * NOM DU FICHER: ListeStatique.java
 * PROVENANT DU PACKAGE: baseDonnees.objets
 * @version: Automne-2020
 *
 * DATE DE CREATION: 2020-10-27
 * DATE DE DERNIERE MODIFICATION: 2020-11-03
 **********************************
 * STRATEGIE GENERALE:
 **********************************
 * La classe ListeStatique est une liste d'objet et son nombre d'éléments
 * significatifs. Elle contient les fonctions necessaires pour la manipuler.
 *
 **********************************
 **HERITAGE(S) DE L'OBJET:
 * - AUCUN
 *
 **AUTRE(S) OBJET(S) UTILISEE(S) DANS L'OBJET:
 * - Aucun
 *
 **MODULE(S) UTILISEE(S) DANS L'OBJET (IMPORTATION):
 * - structures
 *
 ***PARAMETRE(S) DE L'OBJET:
 **CONSTANTE(S):
 * - LONGUEUR_TAB / parametre defaut de la longueur de la liste
 * - ELEMENT_ABSENT / designe nbElements lorsque la liste est vide
 * - MODIFICATEUR_TABLEAU_TROP_LONG / on multiplie la longueur du tableau par
 * 									  cette valeur lorsqu'il est trop long
 * - MODIFICATEUR_TABLEAU_TROP_PETIT / on multiplie la longueur du tableau par
 *    								   cette valeur lorsqu'il est trop petit
 *
 **EXCEPTION(S):
 * - ERR_MAUVAISE_POSITION
 * - ERR_LISTE_VIDE / Lorsque la liste est vide et on tente d'enlever un element
 *
 **ATTRIBUTS(S):
 * - ListeStatique liste / Lorsqu'on indique une position invalide dans la liste
 *
 * --- DEBUT DE L'OBJET ---
 */

package structures;

public class ListeStatique {

	/*==========================================================================
	 *
	 * CONSTANTES
	 *
	 =========================================================================*/
	//region CONSTANTES

	//Creation des messages d'exceptions 
	// Lorsqu'on indique une position invalide dans la liste
	protected static final Exception ERR_MAUVAISE_POSITION  = new Exception(
										" \n *ERREUR* \n"	
										+ "La position indiquee est invalide"
										+ "\n ********************");
	
	// Lorsque la liste est vide et on tente d'enlever un element
	protected static final Exception ERR_LISTE_VIDE = new Exception(
												" \n *ERREUR* \n"
												+ "La liste est vide"
												+ "\n ********************");
	


	// parametre defaut de la longueur de la liste
	protected static final int LONGUEUR_TAB = 100;
	// designe nbElements lorsque la liste est vide 
	protected static final int ELEMENT_ABSENT = -1;

	protected static final double MODIFICATEUR_TABLEAU_TROP_LONG = 0.75;
	protected static final int MODIFICATEUR_TABLEAU_TROP_PETIT = 2;
	//endregion

	/*==========================================================================
	 *
	 * ATTRIBUTS
	 *
	 =========================================================================*/
	//region ATTRIBUTS

	protected Object[] tab;
	protected int nbElements; // designe les elements dans la liste
	//endregion

	/*==========================================================================
	 *
	 * ACCESSEURS
	 *
	 =========================================================================*/
	//region ACCESSEURS
	/***************************
	 * getNbElements
	 * *************************
	 * Accesseur de la constante a avoir lorsqu'un element est absent
	 *
	 * @return la valeur de la constante d'element absent
	 */
	public int getElementAbsent() {
		return ELEMENT_ABSENT;
	}

	/******************************
	 * getNbElements
	 * ****************************
	 * Accesseur du nombre d'elements de la liste
	 *
	 * @return : le nombre delements dans la liste
	 */
	public int getNbElements(){
		return nbElements;
	}

	/******************************
	 * getTab
	 * ****************************
	 * Accesseur du tableau d'objets dans la liste
	 *
	 * @return : tableau d'objets dans la liste
	 */
	public Object[] getTab(){
		return tab;
	}


	/****************************
	 * getErrMauvaisePosition
	 * **************************
	 * Accesseur de l'exeption constante de mauvaise position
	 *
	 * @return : Le message de mauvaise position
	 */
	public static Exception getErrMauvaisePosition(){
		return ERR_MAUVAISE_POSITION;
	}

	/****************************
	 * getErrListeVide
	 * **************************
	 * Accesseur de l'exeption constante de liste vide
	 *
	 * @return : Le message de liste vide
	 */
	public Exception getErrListeVide() {
		return ERR_LISTE_VIDE;
	}

	//endregion

	/*==========================================================================
	 *
	 * CONSTRUCTEURS
	 *
	 =========================================================================*/
	//region CONSTRUCTEURS
	/**
	 * Constructeur par defaut
	 * construit une liste vide
	 */
	public ListeStatique() {

		this.nbElements = 0;
		this.tab = new Object[100];
	}

	/**
	 * Constructeur personaliser
	 * construit une liste d'une longueur recu en parametre
	 *
	 * @param longueurTab : nombre d'elements maximum du tableau construit
	 */
	public ListeStatique(int longueurTab) {
		this.nbElements = 0;
		this.tab = new Object[longueurTab];
	}

	/**
	 * Constructeur par copie
	 * construit la liste selon les parametres
	 *
	 * @param nbElements : le nombre d'elements voulu
	 * @param tab        : le tableau copier
	 */
	public ListeStatique(int nbElements, Object[] tab) {

		this.nbElements = nbElements;
		this.tab = tab;
	}

	/**
	 * Constructeur par copie
	 * construit une copie d'une liste
	 *
	 * @param listStat : liste copier
	 */
	public ListeStatique(ListeStatique listStat) {

		this(listStat.nbElements, listStat.tab);
	}
	
	public Object getElement(int index) {
		return tab[index];
	}
	//endregion

	/*==========================================================================
	 *
	 * METHODES
	 *
	 =========================================================================*/
	//region METHODES
	/**
	 * inserer
	 * **********************
	 * Permet d'inserer un objet dans la liste
	 *
	 * @param element  : Objet a  inserer
	 * @param position : Position de l'insertion dans le tableau
	 * @throws Exception
	 */
	public void inserer(Object element, int position) throws Exception {

		// Augmenter la longueur du tableau si il est plein
		if (this.nbElements >= (this.tab.length -1)){
			
			ajusterLongueurTableau(MODIFICATEUR_TABLEAU_TROP_PETIT);
			
		} else if (this.nbElements < position | position >= (this.tab.length-1)
											| position < 0) {
			
			//Exception: la position est pas valide
			throw ERR_MAUVAISE_POSITION;
		}
		else {
			
			decalerValeursDroite(position);
			
			this.tab[position] = element;
			this.nbElements++;
		}
	}

	/**
	 * InsererApresDernier
	 * **********************
	 * insere un objet passe en parametre a la fin du tableau
	 *
	 * @param element : Objet a inserer
	 */
	public void insererApresDernier(Object element) {

		// Augmenter la longueur du tableau si il est plein
		if (this.nbElements >= this.tab.length){
			ajusterLongueurTableau(MODIFICATEUR_TABLEAU_TROP_PETIT);
			this.tab[this.nbElements] = element;
			this.nbElements++;
		}

		else if (this.nbElements == 0 ) {
			try {
				this.inserer(element, 0);
			} catch (Exception e) {

				e.printStackTrace();
			}


		}
		else {
			this.tab[this.nbElements] = element;
			this.nbElements++;
		}

	}

	/**
	 * supprimerElement
	 * **********************
	 * Supprime un objet dans le tableau
	 *
	 * @param position : Position de l'objet a supprimer
	 * @throws Exception
	 */
	public void supprimerElement(int position) throws Exception {

		// reduire la longueur du tableau si il est a moitie vide
		if (this.nbElements<this.tab.length/2){
			ajusterLongueurTableau(MODIFICATEUR_TABLEAU_TROP_LONG);
		}
		
		if (this.nbElements == 0) {
			//Exception: la liste est vide 
			throw ERR_LISTE_VIDE;
		}
		else if (this.tab[position] == null) {
			//Exception: la position n'est pas valide
			throw ERR_MAUVAISE_POSITION;
		}
		else {
			
			decalerValeursGauche(position);
			
			// Reduit le compteur d'element de 1
			this.nbElements--;

		}
	}

	/*******************************
	 * ajusterLongueurTableau
	 * *****************************
	 * Procedure qui modifie la longueur du tableau d'une liste statique
	 *
	 * @param modificateur : le multiplicateur de la nouvelle longueur
	 */
	public void ajusterLongueurTableau(double modificateur) {
		/**
		 * STRATEGIE
		 * **************
		 * La methode met dans un variable la nouvelle longueur du tableau en 
		 * multipliant la longueur actuelle par un modificateur passe en
		 * parametre. On cree ensuite une copie du tableau à l'aide de la 
		 * nouvelle longueur et on lui assigne toute les valeurs du tableau 
		 * actuelle. On termine en assignant la copie au tableau de la classe.
		 * 
		 */
		// generer un tableau temporaire avec la longueur desire
		Object[] copieTableau = new Object[(int) (this.tab.length * modificateur)];

		// copier les valeurs du tableau a modifier dans le tableau temporaire
		for (int i = 0; i < this.nbElements; i++) {
			copieTableau[i] = this.tab[i];
		}

		// ecraser tableau courrant par le tableau temporaire avec la bonne
		// longueur
		this.tab = copieTableau;
	}

	/**
	 * obtenirIndex
	 * *************
	 * Obtient l'index d'un element recherchee dans le tableau, ou une valeur
	 * negative si l'element ne s'y trouve pas
	 *
	 * @param valeur : Valeur recherchee
	 *
	 * @return Index de l'element chercher. Si il n'est pas present, on retourne
	 * ELEMENT_ABSENT
	 */
	public int obtenirIndex(Object valeur){
		
		int index = ELEMENT_ABSENT; // Index dans le tableau
		
		for(int i = 0; i < nbElements; i++){

			/* On sort de la boucle lorsque l'element est trouve. L'index <<i>>
			   sera egal a l'indice courrant dans la boucles*/
			if(this.tab[i].equals(valeur)){
				index = i;
			}
		}

		// Retour de la reponse
		return index;
	}

	/**
	 * remplacer
	 * *****************************
	 * Remplace une valeur du tableau a un index donne par la valeur donne
	 *
	 * @param valeur : valeur qui va remplacer l'ancienne
	 * @param index : index ou on remplace la valeur
	 * @throws Exception
	 */
	public void remplacer(Object valeur, int index) throws Exception{
		
		// si l'index demander ne contient pas de valeur, on ne peut pas
		// remplacer
		if(index >= this.nbElements){
			
			//Exception: la position est pas valide
			throw ERR_MAUVAISE_POSITION;
		}
		else{
			this.tab[index] = valeur;
		}
		
	}
	
	/**
	 * decalerValeursDroite
	 * ***************************
	 * Decale les valeurs du tableau vers la droite a partir de l'index recu
	 * en parametre
	 * 
	 * @param position
	 */
	public void decalerValeursDroite(int position) {
		
		/**
		 * STRATEGIE
		 * ************************
		 * Boucler au travers du tableau en commencant par l'indice recu en
		 * parametre et remplacer chaque valeur par la valeur precedente dans
		 * le tableau
		 */
		
		for (int i = nbElements; i > position; i--) {
			tab[i] = tab[i - 1];
		}
	}
	
	/**
	 * decalerValeursGauche
	 * ***************************
	 * Decale les valeurs le tableau vers la gauche a partir de l'index recu
	 * en parametre
	 * 
	 * @param position
	 */
	public void decalerValeursGauche(int position) {
		
		/**
		 * STRATEGIE
		 * ************************
		 * Boucler au travers du tableau en commencant par son nombre d'elements
		 * jusqu'a l'indice recu en parametre et remplacer chaque valeur par la 
		 * valeur suivante dans le tableau. Ensuite, retirer la derniere valeur
		 * du tableau
		 */
		
		for (int i = position; i < (nbElements - 1); i++) {
			tab[i] = tab[i + 1];
		}
		
		tab[nbElements] = null;
	}

	//endregion

}