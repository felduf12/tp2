package structures;

/**
 * Un petit programme qui teste les 2 nouvelles methodes de votre liste binaire.
 * Les donnees devraient s'inserer en ordre et vous devriez pouvoir retrouver 
 * une valeur dans la liste.
 * 
 * @author G. Brunet, F. Dufresne, S. Fortin, C. Potvin
 * @version (copyright A2020)
 * 
 */

public class TestListeFouilleEtStatique {

	
	public static void main(String[] args) {
	
	
	// Creation d'une liste statique avec les valeurs par defaut.
	ListeStatique statiqueTestVide1 = new ListeStatique();
	// Creation d'une liste avec longeur predeterminee
	ListeStatique statiqueTestPetite1 = new ListeStatique(10);
		

	
	
	//Debut de la baterie de tests pour la liste par defaut 
	System.out.println("Debut du test pour la liste statique par defaut");
	affichageStatiqueBase(statiqueTestVide1);
	baterieTestStatique(statiqueTestVide1, ListeStatique.LONGUEUR_TAB);
	affichageStatiqueBase(statiqueTestVide1);
	System.out.println("fin du test pour la liste statique par defaut");
	
	//Debut de la baterie de tests pour la petite liste  
	System.out.println("Debut du test pour la liste statique petite");
	affichageStatiqueBase(statiqueTestPetite1);
	baterieTestStatique(statiqueTestPetite1, 10);
	affichageStatiqueBase(statiqueTestPetite1);
	System.out.println("Fin du test pour la liste statique petite");
	
	 // Creation de la liste statique a partir d'une autre liste
	// Declarartion de la liste 
	ListeStatique copieListeAvecModif = new ListeStatique(statiqueTestPetite1);
	
	//Debut de la baterie de tests pour la liste avec modifications
	System.out.println("Debut du test pour la liste avec deja des donnees");
	affichageStatiqueBase(copieListeAvecModif);
	baterieTestStatique(copieListeAvecModif, copieListeAvecModif.nbElements);
	affichageStatiqueBase(copieListeAvecModif);
	System.out.println("Fin du test pour la liste avec deja des donnees");
	

	// Creation de la liste statique a partir d'un obj
	// Declaration des valeurs de la liste 
	Object contenu[] = {10,40,30,63,58,48,79,10,5,3};
	
	// Declarartion de la liste 
	ListeStatique copieListeAvecModif2
						= new ListeStatique((contenu.length -1) , contenu);
	
	//Debut de la baterie de tests pour la liste avec modifications
	System.out.println("Debut du test pour la liste avec deja des donnees "
			+ "d'un obj");
	affichageStatiqueBase(copieListeAvecModif2);
	baterieTestStatique(copieListeAvecModif2, copieListeAvecModif2.nbElements);
	affichageStatiqueBase(copieListeAvecModif2);
	System.out.println("Fin du test pour la liste avec deja des donnees "
			+ "d'un obj");
	
	
	
	//Test Fouille Binaire
	// Creation d'un tableau pour la liste binaire
	Object contenu2[] = {10,40,30,63,58,48,79,10,5,3,-5,12,558};
	//Creation de la liste binaire
	ListeFouilleBinaire binaireTest1 = new ListeFouilleBinaire();
	
	
	// Creation d'une liste binaire 
	
	// Remplissage de la liste binaire 
	for (int i = 0 ;  i < contenu2.length ;i++ ) {
		try {
			binaireTest1.inserer((Comparable)contenu2[i]);
		} catch (Exception e) {
			System.out.println("une erreur");
			e.printStackTrace();
		}
	}
	
	// chercher une valeur dans la liste binaire la valeur 10
	System.out.println(" \n  *** \n "
			+ "Verifier la presence de la  valeur dans la liste binaire ");
	System.out.println(
			 "Chercher la valeur 10 ds la liste");
	System.out.println("Valeur (Presente = 0+) : " 
			 + binaireTest1.obtenirIndex(10));
	
	
	System.out.println(
			 "Fin de la session de teste ");
	}
	
	
	
	
	
	

	 /**************************************
	 * Afficher les informations de base d'une liste statique
	 * ************************************
	 * Sous-Programme # 1
	 * Type de Programme: module 
	 * 
	 * Variable(s) Appelee(s) par le Sous-Programme:
	 * @param ListeStatique
	 *
	 * Variable(s) Retournee(s) par le Sous-Programme:
	 * Aucune 
	 *
	 * 
	 *
	 * --- Debut du Sous-Programme --- 
	 */
	public static void affichageStatiqueBase(ListeStatique statique) {
	    /**********************************
		* STRATEGIE
		*********************************
		* 
		* - Affiche le nombre d'elements que contient la liste presentement 
		*/

		//impression des informations 
		System.out.println("Information de base de la liste statique \n"
			+ "La liste presentement " + statique.nbElements + " elements");
	// ---Fin du Sous-Programme---
	}
	
	 /**************************************
	 * Baterie de tests d'une liste statique 
	 * ************************************
	 * Sous-Programme # 2
	 * Type de Programme: module 
	 * 
	 * 
	 * Variable(s) Appelee(s) par le Sous-Programme:
	 * @param ListeStatique
	 * @param NBmax : nombre maximum d'elements que la liste possede 
	 * Variable(s) Retournee(s) par le Sous-Programme:
	 * Aucune 
	 *
	 * 
	 *
	 * --- Debut du Sous-Programme --- 
	 */
	public static void baterieTestStatique(ListeStatique statique , int NBmax){
	    /**********************************
		* STRATEGIE
		*********************************
		* - tente d'ajouter des valeurs hors de la limite de la liste.
		* - Tente de supprimer des valeurs dans la liste qui est vide.
		*  
		* - Remplit la liste
		* - Tente d'ajouter un element lorsque la liste est pleine 
		*  
		* - Supprime le premier et le dernier element de la liste 
		*  - Tente de supprimer a une position deja vide  
		* 
		* - Chercher la valeur 0 ds la liste
		* - Remplace la valeur du premier element de la liste par 0
		* - Chercher la valeur 0 ds la liste
		* 
		* - Insert une nouvelle valeur au d�but de la liste 
		* 
		* - NE TESTE PAS LE COMPARE TO 
		*/
		
		//Declaration du compteur qui permet de remplir la liste 
		
		System.out.println("DEBUT DE LA BATERIE DE TEST "
				+ "\n Lorsque Tente est ecrit, cela indique que le resultat"
				+ "indique une erreur.");
		
		//1- Tente d'ajouter des valeurs hors de la limite de la liste.
		// Insertion au dela de la liste

		System.out.println("\n*** \n "
				+ "Tente d'ajouter un element au dela de la liste");
		try {
			statique.inserer( 0 , (NBmax +1));
		// impression du message d'erreur 
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		//insertion en dessous de la liste 
		System.out.println("\n *** \n "
				+ "Tente d'ajouter un element en dessous de la liste");
		try {
			statique.inserer( 0 , -1);
		// impression du message d'erreur 
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		// Insertion dans la liste mais ou il n'y a pas encore d'elements
		System.out.println("\n  *** \n "
				+ "Tente d'ajouter un element dans la liste ou c'est vide");
		try {
			statique.inserer( 0 , (NBmax -1));
		// impression du message d'erreur 
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		
		// - Tente de supprimer des valeurs dans la liste qui est vide.
		System.out.println(" \n *** \n Tente de supprimer un element ds une"
				+ " liste vide");
		try {
			statique.supprimerElement(NBmax-1 );
			// impression du message d'erreur 
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		
		// - remplir la liste
		
		System.out.println(" \n *** \n Remplissage de la liste");
		
		for (Integer i =  (NBmax - statique.nbElements); i >= 0 ; i-- ) {
			statique.insererApresDernier(i);
		}
		System.out.println("fin du remplissage");
		
		// - Tente d'ajouter un element lorsque la liste est pleine 
		System.out.println(" \n *** \n Tente d'ajouter un element "
				+ "a la liste pleine");
			try {
				statique.inserer(NBmax, NBmax-1);
				// impression du message d'erreur 
			} catch(Exception e) {
				System.out.println(e.getMessage());
			}
			
		// - Supprime le premier et le dernier element de la liste.
		System.out.println(" \n  *** \n "
				+ "Supprimer le premier et le dernier element de la liste");
		//suppression du dernier element 
		try {
			statique.supprimerElement(NBmax-1);
			// impression du message d'erreur 
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		//suppression du premier element 
		try {
			statique.supprimerElement(0);
			// impression du message d'erreur 
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		// - tenter de supprimer un element hors de la liste 
		
		System.out.println("Supression terminee \n \n  *** \n "
				+ "Tente de supprimer un element hors de la liste");
		try {
			statique.supprimerElement(NBmax-1);
			// impression du message d'erreur 
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		// - Chercher la valeur 0 ds la liste
		System.out.println(" \n  *** \n "
				+ "Chercher la valeur 0 ds la liste");
		System.out.println("Valeur 0 (Absente = -1) : " 
				+ statique.obtenirIndex(0));
		
		//  - Remplace la valeur du premier element de la liste par 0
		
		System.out.println(" \n  *** \n "
				+ "Changer une valeur par 0");		
		try {
			statique.remplacer(0, 0);
			// impression du message d'erreur 
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Changement terminer");
		
		
		// - Chercher ENCORE la valeur 0 ds la liste
		System.out.println(" \n  *** \n "
				+ "Chercher ENCORE  la valeur 0 ds la liste");
		System.out.println("Valeur 0 (Presente = 0+) : " 
				+ statique.obtenirIndex(0));
		
		// - Inserer une valeur au d�but de la liste
		System.out.println(" \n  *** \n "
				+ "Inserer" + NBmax +  " valeur dans la liste");
		statique.insererApresDernier(NBmax);
		System.out.println(" Insertion terminee");
			
		System.out.println(" \n  *** \n "
				+ "Verifier la presence de la nouvelle valeur dans la liste ");
		System.out.println(
				 "Chercher la valeur " + NBmax + " ds la liste");
		System.out.println("Valeur (Presente = 0+) : " 
				 + statique.obtenirIndex(NBmax));
		
		
	// ---Fin du Sous-Programme---
	System.out.println("\n FIN DU TEST \n \n");
	}


	
	
}
