package structures;

/**
 * Classe enfant de ListeStatique permettant de créer une liste contenant les
 * méthodes qui utilise l'algorithme de fouille binaire pour
 * chercher une valeur dans le tableau. 
 * 
 * @author G. Brunet, F. Dufresne, S. Fortin, C. Potvin
 * @version (copyright A2020)
 * 
 */
public class ListeFouilleBinaire extends ListeStatique {

	/**************************************************
    *
    * LES CONSTRUCTEURS
    *
	**************************************************/
	/**
	 * Constructeur par défaut
	 * 
	 * construit une liste vide
	 */
	public ListeFouilleBinaire() {
		
		super();
	}
	
	/**
	 * Constructeur personalise
	 * 
	 * construit une liste d'une longueur recu en parametre
	 *
	 * @param longueurTab : nombre d'elements maximum du tableau construit
	 */
	public ListeFouilleBinaire(int longueurTab) {
		
		super(longueurTab);
	}
	

	/**
	 * obtenirIndex
	 * *************
	 * Obtient l'index d'un element recherche dans le tableau, ou une valeur
	 * negative si l'element ne s'y trouve pas
	 *
	 * @param valeur : Valeur recherchee
	 * 
	 * @return Index de l'element chercher.
	 */
	public int obtenirIndex(Object valeur){
		
		/**
		 * STRATEGIE
		 * *****************
		 * Determiner l'indice au milieu du tableau, si la valeur est plus
		 * grande on garde seulement la partie droite du tableau et si la
		 * valeur est plus petite on garde seulement la partie gauche.
		 * On recommence tant l'indice de fin du tableau est plus grand que
		 * l'indice de gauche.
		 */
		
		int debut = 0; // Index de debut du tableau
		int fin = nbElements - 1; // Index de fin du tableau
		int milieu = 0; // Index du milieu du tableau
		boolean indexTrouve = false; // Determine si la valeur a ete trouve
		Comparable valeurComparable = (Comparable)valeur;
		int resultatComparaison;
		
		while(fin >= debut && !indexTrouve && super.nbElements > 0){
			
			milieu = (fin + debut)/2; // Determiner le milieu du tableau
			
			resultatComparaison = valeurComparable.compareTo(super.tab[milieu]);
				
			// La valeur a ete trouve
			if (resultatComparaison == 0)
				indexTrouve = true;
			
			// La valeur est plus grande que celle du milieu
			else if (resultatComparaison > 0) {
				milieu++;
				debut = milieu ;
			}
			
			// La valeur est plus petite que celle du milieu
			else if (resultatComparaison < 0)
				fin = milieu - 1;
		}
		
		if (super.nbElements > 0 && debut> fin && milieu == 0 &&
				valeurComparable.compareTo(super.tab[0]) > 0 ) {
			milieu = 1;
		}

		/*
		 * Selon le booleen retournerElementAbsent recu en parametre,
		 * on retourne soit ELEMENT_ABSENT ou le dernier index milieu.
		 */
		return indexTrouve ? milieu : -milieu-1 ;
		
	}
	
	/**
	 * inserer
	 * *******************
	 * Insere la valeur dans la liste en ordre croissant selon la valeur de 
	 * retour de la  methode compareTo.
	 * 
	 * @param valeur : Valeur a inserer dans le tableau
	 */
	public void inserer(Object valeur) throws Exception {
		
		/**
		 * STRATEGIE
		 * *******************
		 * Determiner l'index a lequel la valeur doit etre inserer puis decaler
		 * toutes les valeurs plus grandes ou egale a celle-ci vers la droite.
		 * Ensuite, inserer la valeur dans l'index 
		 */
		
		// Obtenir l'index dans lequel on doit inserer la valeur
		
		int index = obtenirIndex(valeur);
		
		
		if (index < 0)
			index = Math.abs(index + 1);
	
		if (index == nbElements)
			super.insererApresDernier(valeur);
		else
			super.inserer(valeur, index);
		
	}

}
