package validations;
/**
 * Classe de test pour la classe modele Utilisateur
 * 
 * @author F. Dufresne, C. Potvin, G. Brunet, S. Fortin
 * @version (copyright A2020)
 */
import baseDonnees.modeles.Utilisateur;

public class TestUtilisateur {

    public static void main(String[] args) {


        /**
         * Creation des utilisateurs bidon.
         */
        Utilisateur test01 = new Utilisateur("Samuel", "okbr", "123456", 20.99);

        Utilisateur test02 = new Utilisateur("Gab", "mamajaifaim", "gf65663", 66);

        Utilisateur test03 = new Utilisateur("Cedric", "codercestcooldesfois", "14gfc", 30.63);

        Utilisateur test04 = new Utilisateur("Felix", "gougougaga", "gptaqbc", 96.96);
    

        /**
         * Test d'athentification avec different parametres
         */
        System.out.println("TESTS AUTHENTIFICATION");
        //Auth. avec des parametres valide. Devrait afficher un TRUE
        System.out.println(test01.authentifier("Samuel", "okbr"));

        //Auth. avec un parametre invalide. Devrait afficher FALSE
        System.out.println(test02.authentifier("Gab", "mamajaiPASfaim"));

        //Auth. avec un l'autre parametre invalide. Devrait afficher FALSE
        System.out.println(test02.authentifier("Brunet", "mamajaifaim"));

        //Auth. avec les deux parametres invalide. Devrait afficher FALSE
        System.out.println(test03.authentifier("Menva", "fairedodo") + "\n");


        /**
         * Test du toString
         */
        //Devrait afficher le nom d'utilisateur, le numero de compte et le solde
        System.out.println("test01: " + test01.toString());
        System.out.println("test02: " + test02.toString());
        System.out.println("test03: " + test03.toString());
        System.out.println("test04: " + test04.toString() + "\n");


        /**
         * Test de difference du solde
         */
        //Devrait afficher 6.99 au solde
        test01.transactionSurSolde(-14);
        System.out.println("Devrait afficher 6.99 au solde: " + 
                            test01.getSolde());

        //Devrait afficher 99 au solde
        test02.transactionSurSolde(33);
        System.out.println("Devrait afficher 99 au solde: " + 
                            test02.getSolde());

        //Devrait afficher 0 au solde.
        test03.transactionSurSolde(-30.63);
        System.out.println("Devrait afficher 0 au solde: " + 
                            test03.getSolde());

        //Devrait afficher -3.04 au solde.
        test04.transactionSurSolde(-100);
        System.out.println("Devrait afficher -3.04 au solde: " + 
                            test04.getSolde() + "\n");

        /**
         * Tests getters
         */
        //Get des nom d'utilisateurs
        System.out.println("NOM" + "\n" + "Test01: " + 
                           test01.getNomUtilisateur() + "\n" +
                           "Test02: " + test02.getNomUtilisateur() + "\n" +
                           "Test03: " + test03.getNomUtilisateur() + "\n" +
                           "Test04: " + test04.getNomUtilisateur() + "\n");

        //Get des numeros de comptes
        System.out.println("#COMPTE" + "\n" + "Test01: " + 
                           test01.getNumeroCompte() + "\n" +
                           "Test02: " + test02.getNumeroCompte() + "\n" +
                           "Test02: " + test03.getNumeroCompte() + "\n" +
                           "Test02: " + test04.getNumeroCompte() + "\n");

        //Get des soldes des comptes
        System.out.println("SOLDE" + "\n" + "Test01: " + 
                            test01.getSolde() + "\n" +
                           "Test02: " + test02.getSolde() + "\n" +
                           "Test03: " + test03.getSolde() + "\n" +
                           "Test04: " + test04.getSolde() + "\n");

        //Get des salts des comptes
        System.out.println("SALTS" + "\n" + "Test01: " + test01.getSalt() + "\n" + 
                           "Test02: " + test02.getSalt() + "\n" + 
                           "Test03: " + test03.getSalt() + "\n" + 
                           "Test04: " + test04.getSalt() + "\n");

        //Get des hash des comptes
        System.out.println("HASHS" + "\n" + "Test01: " + 
                            test01.getHashMotDePasse() + "\n" + 
                           "Test02: " + test02.getHashMotDePasse() + "\n" + 
                           "Test03: " + test03.getHashMotDePasse() + "\n" + 
                           "Test04: " + test04.getHashMotDePasse());
    }
}
