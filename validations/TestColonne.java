//region Entete
/**
 * Programme qui test chacun des constructeurs et chacune des fonctions ecrites
 * dans l'objet Colonne
 *
 * @author G. Brunet, F. Dufresne, S. Fortin, C. Potvin
 * @version (copyright A2020)
 *
 */
//endregion

package validations;
import baseDonnees.objets.Colonne;
import structures.ListeStatique;

public class TestColonne {
    public static void main(String[] args) throws Exception {
        System.out.println("Debut du test de la classe Colonne");

        System.out.println("\nOn cree une nouvelle colonne par constructeur ");
        System.out.println("par defaut. On insere ensuite 7 elements du meme ");
        System.out.println("type et on affiche la colonne.");
        // Constructeurs par defaut==============================================
        Colonne colonneTestee = new Colonne();

        // test ajouterValeur===================================================
        colonneTestee.ajouterValeur("Gab");
        colonneTestee.ajouterValeur("Sam");
        colonneTestee.ajouterValeur("Ced");
        colonneTestee.ajouterValeur("Felix");
        colonneTestee.ajouterValeur("Jean-Gui");
        colonneTestee.ajouterValeur("Yves");
        colonneTestee.ajouterValeur("Gilles");

        // test afficherContenu=================================================
        colonneTestee.afficherContenu();

        // Test obtenirValeur===================================================
        System.out.println("\nTest obtenirValeur");
        System.out.println("On obtient la valeur a l'index 1");
        try{
            System.out.println(colonneTestee.obtenirValeur(1));
        }catch(Exception e){
            e.printStackTrace();
        }

        System.out.println("On tente d'obtenir la valeur a l'index 15 qui" +
                " n'existe pas");
        try{
            System.out.println(colonneTestee.obtenirValeur(15));
        }catch(Exception e){
            e.printStackTrace();
        }

        // Test obtenirIndex====================================================
        System.out.println("\nTest obtenirIndex");
        System.out.println("On tente d'obtenir l'index du nom Gab");
        System.out.println(colonneTestee.obtenirIndex("Gab"));

        System.out.println("On tente d'obtenir l'index du nom Gustave qui " +
                "n'existe pas");
        System.out.println(colonneTestee.obtenirIndex("Gustave"));

        // Test remplacerValeur=================================================
        System.out.println("\nTest remplacerValeur");
        System.out.println("On remplace l'element par Jean a l'index 2");
        colonneTestee.remplacerValeur("Jean", 2);
        colonneTestee.afficherContenu();

        // Test supprimer=======================================================
        System.out.println("\nTest supprimer");
        System.out.println("On supprime l'element a l'index 2");
        try{
            colonneTestee.supprimer(2);
        } catch(Exception e){
            e.printStackTrace();
        }
        colonneTestee.afficherContenu();

        System.out.println("\nOn tente de supprimer a l'index 10 qui est " +
                "vide");
        try{
            colonneTestee.supprimer(10);
        } catch(Exception e){
            e.printStackTrace();
        }
        colonneTestee.afficherContenu();

        // Test des deux autres constructeurs===================================
        System.out.println("\nTest constructeurs");

        System.out.println("On construit une Colonne en copiant la derniere " +
                "Colonne");
        Colonne colonneParCopie = new Colonne(colonneTestee);
        System.out.println("Colonne copiee:");
        colonneParCopie.afficherContenu();

        System.out.println("On construit une nouvelle liste");
        ListeStatique listeAInsererDansColonnne = new ListeStatique();
        listeAInsererDansColonnne.insererApresDernier("Pomme");
        listeAInsererDansColonnne.insererApresDernier("Banane");
        listeAInsererDansColonnne.insererApresDernier("Cantaloup");

        System.out.println("On construit une nouvelle Colonne en utilisant la" +
                " liste comme parametre");
        Colonne colonneParListeStatique =
                new Colonne(listeAInsererDansColonnne);
        colonneParListeStatique.afficherContenu();
    }
}
