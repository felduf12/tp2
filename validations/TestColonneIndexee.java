package validations;

import baseDonnees.objets.ColonneIndexee;

/**
 * Classe utilisee pour effectuer les tests relatifs a la colonne indexee
 * 
 * @author F. Dufresne, C. Potvin, G. Brunet, S. Fortin
 * @version (copyright A2020)
 */
public class TestColonneIndexee {
	public static void main(String[] args) { 
		
		// Colonne indexee qui sera utilise pour les tests
		ColonneIndexee colonne = new ColonneIndexee();
		
		
		// Valeurs qui seront inseres dans la colonne indexee
		String[] fruits = {"Pomme", "Orange", "Banane",
				"Frambroise", "Bleuet"};
		
		
		System.out.println("Creation de la colonne indexee");
		// Test d'ajout de valeurs
		for (int i = 0; i < fruits.length; i++) {
			colonne.ajouterValeur(fruits[i]);
		}
		
		
		System.out.println("Impression des elements de la colonne \n");
		colonne.afficherContenu();
		
		// Test de remplacement
		System.out.println(" \n \n remplacement de fraise (ERREUR))");
		try {
			colonne.remplacerValeur("Fraise", 2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		//Test de suppression
		System.out.println(" \n \n Supression de colonne 2 (banane) ");
		try {
			colonne.supprimer(2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		colonne.afficherContenu();
		
		// Test de recherche d'index
		System.out.println(" \n \n Recherche de l'index de Bleuet ");
		System.out.println(colonne.obtenirIndex("Bleuet"));
		
		System.out.println(" \n \n Recherche de l'index de Orange ");
		System.out.println(colonne.obtenirIndex("Orange"));
		
		// Test de verification de doublons
		System.out.println(" \n \n Bleuet est unique?");
		System.out.println(colonne.estUnique("Bleuet"));
		
		System.out.println(" \n \n Banane est unique?");
		System.out.println(colonne.estUnique("Banane"));
		
	
	}
}
