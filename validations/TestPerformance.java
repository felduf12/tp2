package validations;

import baseDonnees.objets.ColonneIndexee;
import baseDonnees.objets.Colonne;

/**
 * Classe contenant les tests de performances pour la colonne et la colonne
 * indexee
 * 
 * @author F. Dufresne, C. Potvin, G. Brunet, S. Fortin
 * @version (copyright A2020)
 */
public class TestPerformance {
	public static void main(String[] args) { 
		
		//Constantes 
		int GRANDE_COLONNE = 1000; //Grandeur de la colonne (test performance)
		int VALEUR_RECHERCHE = 40; // Multiple de la valeur recherchee
		
		// Colonne indexee qui sera utilise pour les tests
		ColonneIndexee colonneIndexe = new ColonneIndexee();
		
		Colonne colonne = new Colonne();
		
		// remplissage de la colonne avec 1000 valeurs 
		
		for (int i = 1; i <= GRANDE_COLONNE; i++) {
			
			// si la valeur est null, ne pas entree la donne dans la liste  
			if ((i* (-1 * VALEUR_RECHERCHE)) != 0) {
				
				// inserer des valeurs differentes la colonne 
				if (i % 4 == 0) {
					colonne.ajouterValeur(i* (-1 * VALEUR_RECHERCHE));
					colonneIndexe.ajouterValeur(i* (-1 * VALEUR_RECHERCHE));
				}
				else {
					colonne.ajouterValeur(i* VALEUR_RECHERCHE);
					colonneIndexe.ajouterValeur(i* VALEUR_RECHERCHE);
				}
			}
			
		}
		
		
		// Test de performance pour la colonne
		
		//prendre le temps actuel en milisecondes 
		long debut = System.currentTimeMillis();
		
		
		System.out.println("Execution de 1020 recherches dans la colonne...");
		
		//Faire 1020 recherches 
		for (int i = 1; i <= 1020; i++) {
			colonne.obtenirIndex(i*VALEUR_RECHERCHE);
		}
		
		//fin du test Impression du resultat en milisecondes 
		System.out.println(System.currentTimeMillis() - debut + " ms");
		
		// Test de performance pour la colonne indexee
		
		//prendre le temps actuel en milisecondes 
		debut = System.currentTimeMillis();
		
		
		System.out.println
			("\nExecution de 1020 recherches dans la colonne indexee...");
		
		//Faire 1020 recherches 
		for (int i = 1; i <= 1020; i++) {
			colonne.obtenirIndex(i*VALEUR_RECHERCHE);
		}
		
		//fin du test Impression du resultat en milisecondes 
		System.out.println(System.currentTimeMillis() - debut + " ms");
		
		
	
	}
}
