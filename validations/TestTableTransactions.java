/* METHODE DE VALIDATION D'UN OBJET
**************************************************************************
* testTableTransaction 
**************************************************************************
 * PROGRAMMEUR(S) (ETUDIANT(ES)): 
 * @author G. Brunet, F. Dufresne, S. Fortin, C. Potvin
 *
 * COURS: INF111-01
 * TRAVAIL/PROJET: TP02 
 * NUMERO: Partie 2
 *
 * NOM DU FICHER: testTableTransaction.java
 * PROVENANT DU PACKAGE: validations
 * @version: Automne-2020
 * 
 * DATE DE CREATION: 2020-10-28
 * DATE DE DERNIERE MODIFICATION:  2020-10-30
 **********************************
 * STRATEGIE GENERALE:
 **********************************
 * Cette Methode a pour but d'effectuer un test de l'objet tableTransaction.java
 *
 *
 **********************************
 **MODULE(S) UTILISEE(S) DANS LA METHODE (IMPORTATION):
 * - baseDonnees.modeles.Transaction
 * - baseDonnees.objets.TableTransactions
 * --- DEBUT DE LA METHODE ---
*/
package validations;


/**************************************
 * Importation(s)
 *************************************
 *Importation des module(s) utilisee(s) dans le programme 
*/ 

//Importation du module qui permetera l'interaction avec l'objet Transaction
import baseDonnees.modeles.Transaction;

//Importation module qui permetera l'interaction avec l'objet TableTransaction
import baseDonnees.objets.TableTransactions;


public class TestTableTransactions {

	public static void main(String[] args) {
		 /**********************************
		 * STRATEGIE TEST:
		 **********************************
		 * Voici l'ordre avec lequel le test se produira:
		 *
		 * - Creation d'une donnee auquel le statut n'est pas indiquee
		 * - Creation d'une methode auquel la statut est mal indiquee
		 * - Creation d'une valeur auquel le statut est "Accepte"
		 * - Creation d'une valeur auquel le statut est ACCEPTE
		 * - Creation d'une valeur auquel le statut est REFUSE
		 *  - Creation d'une valeur auquel le statut est A_DETERNINER
		 * - (sous-methode) Ajout des transactions dans la table 
		 * - (sous-methode) Afficher les resultats ou "S-TEST-4" est present
		 *
		 **********************************/
		
		
		// Creation des transactions
		Transaction test01 =
				new Transaction("S-TEST-1", "D-TEST-2",  new Double(150.0));
		
		//Creation d'une methode auquel la statut est mal indiquee
		Transaction test02 =
				new Transaction
				("S-TEST-3", "D-TEST-2",  new Double(150.0),"HeyJude");
		
		//Creation d'une valeur auquel le statut est "acceptee"
		Transaction test03 =
				new Transaction
				("S-TEST-3", "D-TEST-3",  new Double(150.0),"Accepte");
		
		//Creation d'une valeur auquel le statut est ACCEPTE
		Transaction test04 =
				new Transaction
				("S-TEST-4","D-TEST-5",new Double(150.0),Transaction.ACCEPTE);
		
		//Creation d'une valeur auquel le statut est REFUSE
		Transaction test05 =
				new Transaction
				("S-TEST-5", "D-TEST-5",  new Double(150.0),Transaction.REFUSE);
		
		//Creation d'une valeur auquel le statut est A_DETERNINER
		Transaction test06 =
				new Transaction
				("S-TEST6","D-TEST6",new Double(5.0),Transaction.A_DETERMINER);
		

		
		//Creation de la table des transactions 
		
		TableTransactions testTable = new TableTransactions();
		
		//ajout des transactions dans la table 
		testTable.ajouterUneTransaction(test01);
		testTable.ajouterUneTransaction(test02);
		testTable.ajouterUneTransaction(test03);
		testTable.ajouterUneTransaction(test04);
		testTable.ajouterUneTransaction(test05);
		testTable.ajouterUneTransaction(test06);

		//testTable.obtenirTransactionsPourCompte("S-TEST-4");
		
	}
}
