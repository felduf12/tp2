/** OBJET
**************************************************************************
*Transaction
**************************************************************************
 * PROGRAMMEUR(S) (ETUDIANT(ES)): 
 * @author G. Brunet, F. Dufresne, S. Fortin, C. Potvin
 *
 * COURS: INF111-01
 * TRAVAIL/PROJET: TP02
 * NUMERO: Partie 02
 *
 * NOM DU FICHER: Transaction.java
 * PROVENANT DU PACKAGE: baseDonnees.objets
 * @version: Automne-2020
 * 
 * DATE DE CREATION: 2020-10-27
 * DATE DE DERNIERE MODIFICATION: 2020-10-27
 **********************************
 * STRATEGIE GENERALE:
 **********************************
 * La classe Transaction represente une transaction 
 * bancaire entre deux utilisateurs.
 *
 * Cet objet sera utilisee dans le 
 * module principal ou le(s) sous-module(s).
 *
 **********************************
 **HERITAGE(S) DE L'OBJET:
 * - AUCUN 
 * 
 **AUTRE(S) OBJET(S) UTILISEE(S) DANS L'OBJET:
 * - AUCUN
 * 
 **MODULE(S) UTILISEE(S) DANS L'OBJET (IMPORTATION):
 * - AUCUN
 *
 ***PARAMETRE(S) DE L'OBJET:
 **CONSTANTE(S):
 * - ACCEPTE = String / Message "Accepte"
 * - REFUSE = String / Message "Refuse"
 * - A_DETERMINER = String / Message "A determiner"
 * 
 **Attribut(S):
 * - noCompteSource = String / Nom du compte qui donne le montant  
 * - noCompteDestination = String / Nom du compte qui recoit le montant
 * - montant = double / Montant de la transaction en dolars 
 * - statut = String / Statut actuel de la transaction 
 *
 * --- DEBUT DE L'OBJET ---
**/

package baseDonnees.modeles;

//Debut de la classe principale de l'objet avec aucun heritage
public class Transaction {

	
	
	//Message(s) en lien avec l'objet
	
	//Message de statut de transation completee avec succes 
	public static String ACCEPTE = "Accepte"; 
	
	//Message de statut de transaction annulee 
	public static String REFUSE  = "Refuse";
	
	//Message de transaction incomplete pour l'instant 
	public static String A_DETERMINER  = "A determiner";
	
	//*Declaration(s) de(s) EXCEPTION(S) en lien avec l'objet
	//TODO
	
	//*Declaration(s) de(s) VARIABLE(S) en lien avec l'objet 
	private String noCompteSource; // Nom du compte qui donne le montant  
	private String noCompteDestination; // Nom du compte qui recoit le montant
	private double montant; // Montant de la transaction en dolars 
	private String statut; // Statut actuel de la transaction 
	
	/**************************************
	* Constructeur(s)
	*************************************
	* Losqu'un constructeur est appelee, un nouvel objet est cr�e 
	* et des valeurs y sont inscrites. 
	*
	*
	* Constructeur(s) utilisees avec l'objet:
	*
	* - Constructeur avec TOUS les parametres : :Lorsqu'il est appelee avec 
	*									tous les param�tres indiquees.
	*
	* - Constructeur avec PRESQUE TOUS les parametres :
	* 		 :Lorsqu'il est appelee avec tous les param�tres sauf status.
	*/
	
	/**	
	*	---	Constructeur avec TOUS les parametres	---
	*			Initialise l'objet avec le statut.
	**/
	public Transaction( String entreeNoCompteSource,
						String entreeNoCompteDestination,
						double entreeMontant,
						String entreeStatut){
		
		// Assigne les valeurs a l'objet 
		this.noCompteSource = entreeNoCompteSource;
		this.noCompteDestination = entreeNoCompteDestination;
		this.montant = entreeMontant;
		
		//appeler la sous-methode qui mute le statut de la transaction
		this.setStatut(entreeStatut);

	}
	
	
	/*	
	*	---	Constructeur avec PRESQUE TOUS les parametres	---
	*			Initialise l'objet sans le statut.
	*/
	public Transaction( String entreeNoCompteSource,
						String entreeNoCompteDestination,
						double entreeMontant){
		/**********************************
		 * STRATEGIE DU CONSTRUCTEUR: 
		 *********************************
		 * Appelle le constructeur avec TOUS les parametres et indique 
		 * la valeur A_DETERMINER comme valeur de statut 
		*/
		this(entreeNoCompteSource,
				entreeNoCompteDestination,
				entreeMontant,
				A_DETERMINER);
	}


	/**************************************
	* Accesseur(s) INFORMATEUR(S) /Getter(s)
	*************************************
	* Outil qui offre l'access aux donnees de l'objet a partir de l'exterieure.
	*	
	* Accesseur(s) utilisees avec l'objet:
	* -getNoCompteSource:Retourne la valeur du compte source en String 
	* -getNoCompteDestination:Retourne la valeur du compte destination en String 
	* -getMontant:Retourne la valeur du montant en double. 
	* -getStatus: Retourne la valeur du statut en String. 
	* -getTransaction: Retourne toutes les informations de la transaction
	* 													(equivalent de toString)
	*/
	
	/*	
	*	---	Accesseur getNoCompteSource	---
	* Accede et retourne la valeur du compte source en String. 
	*
	* --
	* Retourne: 
	* - noCompteSource
	*/
	public String getNoCompteSource(){
		return this.noCompteSource;
	}
	
	/*	
	*	---	Accesseur getNoCompteSource	---
	* Accede et retourne la valeur du compte destination en String. 
	*
	* --
	* Retourne: 
	* - noCompteDestination
	*/
	public String getNoCompteDestination(){
		return this.noCompteDestination;
	}
	
	/*	
	*	---	Accesseur getMontant	---
	* Accede et retourne la valeur du montant en double. 
	*
	* --
	* Retourne: 
	* - montant
	*/
	public double getMontant(){
		return this.montant;
	}
	
	/*	
	*	---	Accesseur getStatus	---
	* Accede et retourne la valeur du statut en String. 
	*
	* --
	* Retourne: 
	* - statut
	*/
	public String getStatus(){
		return this.statut;
	}
	
	/*	
	*	---	Accesseur getTransaction	---
	* Accede et retourne la valeur du complete de la transaction en String. 
	*
	* --
	* Retourne: 
	* - noCompteSource, noCompteDestination, montant et statut
	*/
	public void getTransaction(){
		/**********************************
		 * STRATEGIE DE LA SOUS-METHODE: 
		 *********************************
		 * - Imprime un String avec toutes les valeurs de la transaction  
		*/

		 
		//Affichage des valeurs de la transaction 
		System.out.println("Cpte source = " + this.getNoCompteSource() + 
				" / Cpte destination = " + this.getNoCompteDestination() + 
				" / Montant = " + this.getMontant() + 
				" / Statut = " + this.getStatus());

		
	}
	
	/**************************************
	* Mutateur(s)/Setter(s)
	*************************************
	*	Outil qui permet l'access et la modification des donnees de l'objet 
	*										a partir de l'exterieure.
	*	
	* Mutateur(s) utilis�es avec l'enregistrement:
	* - setStatut :	Change la valeur statut avec la valeur indiqu�e en String. 
	* 
	*/
	
	
	/**	 
	*	---	Mutateur setStatut	---
	* Accede et change la valeur statut dans l'objet
	*								     avec la valeur entree.
	*
	* --
	* Transforme: 
	* - statut = String		permet seulements les constantes ACCEPTE, REFUSE
	*  															 ou A_DETERMINER
	**/
	public void setStatut(String entreeStatut){
	// Si la valeur entree est "Accepte" ou "Refuse"
		if (entreeStatut == ACCEPTE | entreeStatut == REFUSE) {
			// Le changement de valeur se produit
			this.statut = entreeStatut;
		}
		
	// Sinon 
		else {
			//Indiquer le statut comme "A determiner"
			this.statut = A_DETERMINER;
		}
	}
	
}
