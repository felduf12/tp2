package baseDonnees.modeles;

/**
 * Classe du modele des utilisateurs 
 * 
 * @author Samuel 
 * @version (copyright A2020)
 */
import java.util.Arrays;
import baseDonnees.utils.UtilitairesDB;

public class Utilisateur {
    private String nomUtilisateur; // Nom de l'utilisateur
    private String numeroCompte; // Numero de compte
    private double solde; // Solde du compte
    private byte[] salt; // Salt associe au compte
    private byte[] hashMotDePasse; // Hash du mot de passe de l'utilisateur
    
    /**
     * Constructuer qui initialise tout a partir des parametres 
     * sauf le salt et le hash du mot de passe. Ils sont cree a l'aide des 
     * methodes de UtilitairesDB.
     * 
     * @param nomUtilisateur Nom d'utilisateur
     * @param motDePasse Mot de passe
     * @param numeroDeCompte Numero du compte
     * @param solde Solde du compte
     */
    public Utilisateur(String nomUtilisateur,
                        String motDePasse,
                        String numeroDeCompte,
                        double solde){

        this.nomUtilisateur = nomUtilisateur;
        this.numeroCompte = numeroDeCompte;
        this.solde = solde;
        this.salt = UtilitairesDB.obtenirSalt();
        this.hashMotDePasse = UtilitairesDB.hashMotDePasse(motDePasse, salt);
    }
    /**
     * Constructeur d'utilisateur utilisant les valeurs recues.
     * 
     * @param nomUtilisateur Nom d'utilisateur
     * @param hashMotDePasse Mot de passe hache
     * @param salt Salt pour le hash et la decryption
     * @param numeroDeCompte Numero de compte
     * @param solde Solde du compte
     */
    public Utilisateur(String nomUtilisateur,
                        byte[] hashMotDePasse,
                        byte[] salt,
                        String numeroDeCompte,
                        double solde){

        this.nomUtilisateur = nomUtilisateur;
        this.numeroCompte = numeroDeCompte;
        this.solde = solde;
        this.salt = salt;
        this.hashMotDePasse = hashMotDePasse;
    }

    /**
     * Methode qui retourne un boolean selon si oui (true) ou non (non) les 
     * donnees passé en parametre correspondent au valeur des attributs.
     * 
     * @param nomUtilisateur Nom d'utilisateur entre
     * @param motDePasse Mot de passe entre
     * @return boolean de confirmation
     */
    public boolean authentifier(String nomUtilisateur, String motDePasse){
        boolean confirmation = false;

        /**
         * STRATEGIE (dans un contexte réel)
         * 
         * Je dois trouver le nom d'utilisateur dans la BD a celui correspondant
         * a celui entre par l'utilisateur. Je trouve ensuite le salt associe au
         * compte et je hash ensuite le mot de passe entre par l'utilisateur a 
         * l'aide de se dernier. Si le hash du mot de passe resultant est le 
         * meme que celui associé au nom d'utilisateur, je retourne TRUE. Sinon,
         * FALSE.
         */
        if(nomUtilisateur == this.nomUtilisateur && 
         Arrays.equals(UtilitairesDB.hashMotDePasse(motDePasse, salt), hashMotDePasse)){
            confirmation = true;
        }

        return confirmation;
    }

    /**
     * Methode qui retourne une chaine de characteres representant l'objet
     * utilisateur choisi.
     * 
     * @return string 
     */
    public String toString(){
        return "\n" + "Nom Utilisateur: " + nomUtilisateur + "\n" + 
                "Numero de compte: " + numeroCompte + "\n" + 
                "Solde: " + solde + "\n";
    }

    /**
     * Ajoute la difference au solde de l'utilisateur.
     * 
     * @param differentiel Montant a ajouter.
     */
    public void transactionSurSolde(double differentiel){
        this.solde = this.solde + differentiel;
    }

    /**
     * Accesseur du nom d'utilisateur
     * @return nom d'utilisateur
     */
    public String getNomUtilisateur() {
        return nomUtilisateur;
    }

    /**
     * Accesseur du numero de compte
     * @return numero de compte
     */
    public String getNumeroCompte() {
        return numeroCompte;
    }

    /**
     * Accesseur du solde du compte
     * @return solde
     */
    public double getSolde() {
        return solde;
    }

    /**
     * Accesseur du salt du compte
     * @return salt
     */
    public byte[] getSalt() {
        return salt;
    }

    /**
     * Accesseur du hash du compte
     * @return hash
     */
    public byte[] getHashMotDePasse() {
        return hashMotDePasse;
    }
}
