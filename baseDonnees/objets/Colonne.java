//region Entete
/**************************************************************************
 *Colonne
 **************************************************************************
 * PROGRAMMEUR(S) (ETUDIANT(ES)):
 * @author G. Brunet, F. Dufresne, S. Fortin, C. Potvin
 *
 * COURS: INF111-01
 * TRAVAIL/PROJET: TP02
 * NUMERO: Partie 02
 *
 * NOM DU FICHER: Colonne.java
 * PROVENANT DU PACKAGE: baseDonnees.objets
 * @version: Automne-2020
 *
 * DATE DE CREATION: 2020-10-27
 * DATE DE DERNIERE MODIFICATION: 2020-11-03
 **********************************
 * STRATEGIE GENERALE:
 **********************************
 * La classe Colonne comprend une listeStatique comme attribut et utilise ses
 * methodes pour manipuler les elements de la liste.
 *
 **********************************
 **HERITAGE(S) DE L'OBJET:
 * - AUCUN
 *
 **AUTRE(S) OBJET(S) UTILISEE(S) DANS L'OBJET:
 * - InterfaceColonne
 *
 **MODULE(S) UTILISEE(S) DANS L'OBJET (IMPORTATION):
 * - baseDonnees.bases.InterfaceColonne
 * - structures.ListeStatique
 *
 ***PARAMETRE(S) DE L'OBJET:
 **CONSTANTE(S):
 * - Aucun
 *
 **EXCEPTION(S):
 * - Aucune
 *
 **ATTRIBUTS(S):
 * - ListeStatique liste;
 *
 * --- DEBUT DE L'OBJET ---
 */
//endregion

package baseDonnees.objets;

import baseDonnees.bases.InterfaceColonne;
import structures.ListeStatique;

public class Colonne implements InterfaceColonne {
	/*==========================================================================
	 *
	 * ATTRIBUT
	 *
	 =========================================================================*/
	//region ATTRIBUT

	protected ListeStatique liste;

	//endregion

	/*==========================================================================
	 *
	 * CONSTRUCTEURS
	 *
	 =========================================================================*/
	//region CONSTRUCTEURS


	/****************************
	 * Constructeur par default
	 * **************************
	 * Initie une colonne avec comme attribut une liste par default.
	 *
	 */
	public Colonne(){
		this.liste = new ListeStatique();
	}

	/**************************************
	 * Constructeur par copie
	 * ************************************
	 * Constructeur qui copie une autre colonne
	 *
	 * @param colonneACopier : colonne qu'on copie pour en creer une nouvelle
	 */
	public Colonne(Colonne colonneACopier){
		this.liste = colonneACopier.liste;
	}

	/***********************************
	 * Constructeur par parametre
	 * ********************************
	 * Constructeur qui prend une liste et la met dans la colonne
	 *
	 * @param liste : Liste a inserer dans la colonne
	 */
	public Colonne(ListeStatique liste) {
		this.liste = liste;
	}
	//==========================================================================
	//endregion


	/*==========================================================================
	 *
	 * SERVICES_OFFERTS
	 *
	 =========================================================================*/
	//region SERVICES_OFFERTS

	/**
	 * Ajoute la valeur a la fin de la colonne.
	 * 
	 * La colonne est dynamique, La limite de valeurs est dependante 
	 * de la taille de la memoire.
	 * 
	 * @param valeur La valeur a ajouter.
	 */
	public void ajouterValeur(Object valeur) {
		this.liste.insererApresDernier(valeur);
	}

	/**
	 * Retourne la valeur situee a l'index fourni.
	 * 
	 * L'index doit etre valide.
	 * 
	 * @param index La position de la valeur voulue.
	 * @return La valeur situe a l'index, une copie reste dans la liste.
	 * @throws Exception index < 0 ou index > getNbElements()
	 */
	public Object obtenirValeur(int index) throws Exception {
		if(this.liste.getNbElements() < 0 ||
				index > this.liste.getNbElements()){

			throw ListeStatique.getErrMauvaisePosition();
		}
		return this.liste.getElement(index);
	}
	
	/**
	 * Retourne la position de la valeur qui doit etre Comparable et
	 * implementer compareTo.  Si la valeur est absente, la fonction
	 * retourne ELEMENT_ABSENT
	 * 
	 * @param valeur La valeur cherchee
	 * @return La position de la valeur ou ELEMENT_ABSENT
	 */
	public int obtenirIndex(Object valeur) {
		return this.liste.obtenirIndex(valeur);
	}
	
	/**
	 * Permet de remplace une valeur a la position fourni.
	 * 
	 * L'index doit etre valide.
	 * @param index
	 * @param valeur	 
	 * @throws Exception index < 0 ou index > getNbElements()
	 */
	public void remplacerValeur(Object valeur, int index) throws Exception {
		if(this.liste.getNbElements() < 0 ||
				index > this.liste.getNbElements()){

			throw ListeStatique.getErrMauvaisePosition();
		}
		this.liste.remplacer(valeur, index);
	}

	/**
	 * Supprimer la valeur situee a la position fourni par l'index.
	 *
	 * Si la moitie de la liste est inutilisee, elle est reduite du quart
	 * de sa taille.
	 *
	 * @param index La position a supprimer.
	 * @throws Exception index < 0 ou index > getNbElements()
	 */
	public void supprimer(int index) throws Exception {
		if(this.liste.getNbElements() < 0 ||
				index > this.liste.getNbElements()){

			throw ListeStatique.getErrMauvaisePosition();
		}

		this.liste.supprimerElement(index);
	}

	/**
	 * Retourne le nombre d'elements actuellement dans la liste
	 * 
	 * @return Le nombre d'elements dans la liste.
	 */
	public int getNbElements() {
		return this.liste.getNbElements();
	}
	
	/**
	 * Sert principalement au debogage lors des tests.  Il affiche les
	 * valeurs de la liste dans la console.
	 */
	public void afficherContenu() {
		System.out.println("Elements de la Colonne: ");
		System.out.println("INDEX     VALEUR");
		for(int i = 0; i < this.getNbElements(); i++){
			System.out.println(i + "         " + this.liste.getTab()[i]);
		}
	}
	//endregion

}