/* OBJET
**************************************************************************
* TableTransaction
**************************************************************************
 * PROGRAMMEUR(S) (ETUDIANT(ES)): 
 * @author G. Brunet, F. Dufresne, S. Fortin, C. Potvin
 *
 * COURS: INF111-01
 * TRAVAIL/PROJET: TP02
 * NUMERO: Partie 02
 *
 * NOM DU FICHER: TableTransactions.java
 * PROVENANT DU PACKAGE: baseDonnees.objets
 * @version: Automne-2020
 * 
 * DATE DE CREATION: 2020-10-27
 * DATE DE DERNIERE MODIFICATION: 2020-10-27
 **********************************
 * STRATEGIE GENERALE:
 **********************************
 * Cet objet a pour but de regrouper plusieurs transactions dans une liste.
 *
 * Cet objet sera utilisee dans le 
 * module principal ou le(s) sous-module(s).
 *
 **********************************
 **MODULE(S) UTILISEE(S) DANS L'OBJET (IMPORTATION):
 * - baseDonnees.modeles.Transaction
 *
 **VARIABLE(S):
 * @param transaction[] = Objet / Liste de transaction 
 *
 * --- DEBUT DE L'OBJET ---
*/

package baseDonnees.objets;

//Importation du module qui permetera l'interaction avec l'objet Transaction
import baseDonnees.modeles.Transaction;

//Debut de la classe principale de l'objet avec aucun heritage
public class TableTransactions{

	//*Declaration(s) de(s) VARIABLE(S) en lien avec l'objet 
	//private Object[] transactionss = new Object[1];
	private Transaction[] transaction = null;
	
	/**************************************
	* Operateur(s)
	*************************************
	*	Lorsqu'un operateurs est appelee, 
	*	une action se produit et un resultat est retourne.
	*
	* Operateur(s) utilisees avec l'objet:
	*
	* - ajouterUneTransaction:	Insert une nouvelle transaction dans la liste 
	*
	* - obtenirTransactionsPourCompte: Cherche toutes les transactions dans la 
	* 									liste qui sont  en lien avec un compte 
	* 									en particulier et affiche le resultat
	*/
	
	/*	
	*	---	Operateur ajouterUneTransaction	---
	* Insert une nouvelle transaction dans la liste 
	*
	* --
	* Utilise: 
	* - une transaction deja existante et l'insert dans la liste 
	*/
	public void  ajouterUneTransaction(Transaction nouvelleTransaction){
		
		//	Declaration(s) de(s) variable(s)
		// Creation de la nouvelle table avec la longeur appropriee
		Object[] tableTemp = new Object[this.transaction.length];
		
		//Pour chacun des elements dans le tableau des transactions
		for (int i = 0 ; i <= (this.transaction.length -1); i++ ) {
			//transferer le tout dans le tableau temporaire de transaction 
			tableTemp[i] = this.transaction[i];
		}
		//Insertion de la nouvelle transaction dans le tableau temporaire 
		tableTemp[(tableTemp.length -1 )] = nouvelleTransaction;
		
		//Appliquer la table temporaire dans la table permanente 
		this.transaction = ((Transaction[])tableTemp);
		}
	
	/*	
	*	---	Operateur obtenirTransactionsPourCompte	---
    * Cherche toutes les transactions dans la 
	* liste qui sont  en lien avec un compte 
	* en particulier et affiche le resultat
	* --
	*/
	
	public void obtenirTransactionsPourCompte(String nomDeCompte){
		//	Declaration(s) de(s) variable(s)
		// Creation de la liste des transations provenant du compte
		TableTransactions transactionSource = new TableTransactions();

		// Creation de la liste des transactions recu par le compte 
		TableTransactions transactionDestination = new TableTransactions();

		
		//Pour chaque element dans la table de transactions 
		for (int i = 0; i <= (this.transaction.length -1) ; i++   ) {
			//si la transaction provient du compte
			if (nomDeCompte == 
					((Transaction) this.transaction[i]).getNoCompteSource()) {
				
				// Ajouter la transaction dans la table source
					transactionSource.ajouterUneTransaction(
						(Transaction) this.transaction[i]);
			}
			
				//si la transaction est recu par le compte
				if (nomDeCompte == 
				((Transaction) this.transaction[i]).getNoCompteDestination()){
					
					// Ajouter la transaction dans la table source
						transactionDestination.ajouterUneTransaction(
							(Transaction) this.transaction[i]);
				
			}	 
		}

		// Affichage du resultat 
		System.out.println("resultat de la recherche en lien avec le compte " +
		 nomDeCompte);
		
			
			//afficher les transactions provenant du compte source 
			System.out.println("Transaction(s) dont le compte source est " +
			nomDeCompte);
			
			//Appeler le sous programme qui affiche les resultats 
			transactionSource.afficherTransactions();
			
			//afficher les transactions provenant du compte destination 
			System.out.println("Transaction(s) dont le compte destination est " +
			nomDeCompte);
			
			//Appeler le sous programme qui affiche les resultats 
			transactionDestination.afficherTransactions();

	}
	
	
	//
	/*SOUS-METHODE
	 **************************************
	 * afficherTransactions
	 * ************************************
	 * SOUS-METHODE #
	 * TYPE DE SOUS-METHODE: MODULE 
	 * 
	 * VALEUR(S) APPELEE(S) PAR LE SOUS-METHODE:
	 * 
	 * --- DEBUT DE LA SOUS-METHODE --- 
	*/
	private void afficherTransactions() {
		 
		/**********************************
		 * STRATEGIE DE LA SOUS-METHODE: 
		 *********************************
		 * - Sous-methode qui affiche chacune des transations dans une table
		 *  
		 * - La sous-methode utilise la boucle for 
		 * 
		*/
		
		//pour chacune des transactions dans la table
		for (int i = 0; i < this.transaction.length -1; i++) {
			
			// si la transaction n'est pas null 
			if (((Transaction) this.transaction[i]) != null) {
				
				//afficher le resultat de la transactions 

				((Transaction) this.transaction[i]).getTransaction();
			}
		}

		// ---FIN DE LA SOUS-METHODE---
	 	}
}
