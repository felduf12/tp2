package baseDonnees.objets;

import structures.ListeFouilleBinaire;

/**
 * Objet qui contient une valeur et son index dans un tableau.
 * Sera utilise comme case dans la colonneIndexee.
 * 
 * @author G. Brunet, F. Dufresne, S. Fortin, C. Potvin
 * @version (copyright A2020)
 *
 */
public class ColonneIndexee extends Colonne {
	
	//Creation des messages d'exceptions 
	// Lorsqu'on indique une position invalide dans la liste
	protected static final Exception ERR_SUPPORT_REMPLACER  = new Exception(
										" \n *ERREUR* \n"	
										+ "Le remplacement de valeur n'est pas"
									    + " supporte dans une colonne indexee"
										+ "\n ********************");
	
	ListeFouilleBinaire listeIndex;
	
	public ColonneIndexee() {
		super();
		listeIndex = new ListeFouilleBinaire();
	}
	
	/**
	 * Ajoute un element dans le tableau
	 */
	public void ajouterValeur(Object valeur) {
		
		/**
		 * STRATEGIE
		 * *******************
		 * Ajouter la valeur dans la liste statique puis instancier un nouvel
		 * element contenant la valeur et l'index de cette valeur dans la
		 * liste statique dans la liste fouille binaire
		 */
		
		super.ajouterValeur(valeur);
		
		//Instancier le nouvelle element avec la valeur recu en parametre
		Element element = new Element(getNbElements() - 1, (Comparable)valeur);
		try {
			
			//Inserer le nouvel element dans la colonneIndexee
			listeIndex.inserer(element);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Retourne la position de la valeur qui doit etre Comparable et
	 * implementer compareTo.  Si la valeur est absente, la fonction
	 * retourne ELEMENT_ABSENT
	 * 
	 * @param valeur La valeur cherchee
	 * @return La position de la valeur ou ELEMENT_ABSENT
	 */
	public int obtenirIndex(Object valeur) {
		
		/**
		 * STRATEGIE
		 * *******************
		 * Trouver l'element contenant la valeur recu en parametre dans 
		 * la colonneIndexee puis retourner l'index qui est contenu dans cet
		 * element
		 */
		
		// Trouver la valeur dans la colonne indexee
		int indexBinaire = super.obtenirIndex(valeur);
		int index; // Index a retourner
		
		// Si la valeur a ete trouvee
		if (indexBinaire >= 0) {
			
			// Retourner l'index contenu dans l'element trouve
			Element element = (Element)listeIndex.getElement(indexBinaire);
			index = element.index;
		}
		else
			// Sinon, retourner ELEMENT_ABSENT
			index = listeIndex.getElementAbsent();
		
		return index;
	}
	
	/**
	 * Le remplacement de valeur n'est pas supporte dans une colonne indexee 
	 * 
	 * @throws Fonction n'est pas supporte
	 */
	public void remplacerValeur(Object valeur, int index) throws Exception {
		throw ERR_SUPPORT_REMPLACER;
	}
	
	/**
	 * Supprimer la valeur situee a la position fourni par l'index.
	 * 
	 * @param index La position a supprimer.
	 * @throws Exception index < 0 ou index > getNbElements()
	 */
	public void supprimer(int index) throws Exception {	
		
		/**
		 * STRATEGIE
		 * *********************
		 * Supprimer l'element dans la liste statique a l'index recu en
		 * parametre. Trouver l'element contenant l'index recu en parametre
		 * puis le supprimer de la liste fouille binaire.
		 */
		
		super.supprimer(index);
		
		try {
			listeIndex.supprimerElement(decalerIndex(index));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Decale les index des element dans la colonneIndexee 
	 * 
	 * @param index
	 * @return
	 */
	public int decalerIndex(int index) {
		/**
		 * STRATEGIE 
		 * *******************
		 * Boucler au travers le tableau et decrementer les index plus
		 * grand que l'index reu en parametre. Retourner la position de 
		 * l'element qui contient l'index re�u en parametre.
		 */
		
		//Position de l'element contenant l'index
		int indexListeBinaire = 0;
		
		for (int i = 0; i < listeIndex.getNbElements(); i++) {
			Element element = (Element)listeIndex.getElement(i);
			
			/** 
			 * Garder la position de l'element contenant l'index re�u en 
			 * parametre en memoire 
			 */
			if (element.index == index) {
				indexListeBinaire = i;
			}
			else {
				
				//Decrementer les index plus grand que celui re�u en parametre
				if (element.index > i)
					element.index--;
			}
		}
		return indexListeBinaire;
		
	}
	
	@Override 
	public void afficherContenu() {
		 /**********************************
		 * STRATEGIE:
		 **********************************
		 * Afficher le contenu de la colonne indexee en string. 
		 **********************************/
		

		//Impression de chacun des elements de la liste 
		for (int i = 0; i < this.getNbElements(); i++) {
			System.out.println(this.listeIndex.getElement(i));	
		}
	}
	
	/**
	 * Verifie si la valeur passee en parametre est deja dans le tableau.
	 * Retourne true si la valeur n'est pas presente dans le tableau.
	 * 
	 * @param valeur
	 * @return
	 */
	public boolean estUnique(Object valeur) {
		return obtenirIndex(valeur) == listeIndex.getElementAbsent();
	}
	
	/**
	 * Classe interne representant un element dans la colonne indexee.
	 * 
	 * @author Felix Dufresne
	 *
	 */
	private class Element implements Comparable<Element> {
		private int index; // Index dans le tableau
		private Comparable valeur; // Valeur de la case
		
		/**
		 * Constructeur par parametre
		 * 
		 * @param index
		 * @param valeur
		 */
		public Element(int index, Comparable valeur) {
			this.index = index;
			this.valeur = valeur;
		}
		
		/**
		 * Retourne une chaine de charactere contenant les informations de
		 * l'element
		 */
		public String toString() {
			return "Index: " + index + "; Valeur: " + valeur.toString();
		}
		
		/**
		* Doit retourner un nombre negatif si la valeur actuelle est plus 
		* petite que la valeur recue.  
		* Doit retourner un nombre positif si la valeur actuelle est 
		* plus grande que la valeur recue et doit retourner 0 si elles 
		* sont egales.
		*/
		public int compareTo(Element element) {
			return this.valeur.compareTo(element.valeur);
		}
	}

}
